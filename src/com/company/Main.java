package com.company;

public class Main {

    public static void main(String[] args) {
        int[] nums = new int[4];
        int l = nums.length;
        for(int i = 0; i < l; i++){
            nums[i] = (int) (Math.random() * 11  - 5);
        }
        ArrSort arrSort = new ArrSort(nums);
        arrSort.getSortedArr();
    }
}
