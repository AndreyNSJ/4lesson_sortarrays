package com.company;

public class ArrSort {
    private int[] nums;

    public ArrSort(int[] nums) {
        this.nums = nums;
    }

    public void getSortedArr(){
        int current, i, j, l = nums.length;
        for(i = 0; i < l; i++){
            if(nums[i] % 2 != 0){
                j = l;
                do{
                    if(j > i)
                        j--;
                    else break;
                }while(nums[j] % 2 != 0);
                current = nums[j];
                nums[j] = nums[i];
                nums[i] = current;
            }
        }
        for(i = 0; i < l; i++){
            System.out.print(nums[i] + " ");
        }
    }
}
